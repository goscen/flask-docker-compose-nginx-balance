from flask import Flask
from socket import gethostname
app = Flask(__name__)

@app.route("/")
def hi():
    return f"<p><h3>Hello world from {gethostname()}</h3><p>"

if __name__ == "__main__":
    app.run(debug=True)
